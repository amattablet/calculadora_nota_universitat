mod app;
mod estat;
mod calculs;

use app::App;
use calculs::calcular_nota_entrada_universitat;
use estat::{SegonBatxillerat, CursatBatxillerat, Selectivtat, TDR, PrimerBatxillerat};

fn main() {
    dbg!(
        calcular_nota_entrada_universitat(PrimerBatxillerat::default(), SegonBatxillerat::default(), Selectivtat::default(), TDR::default(), &[0.0, 0.0, 0.1, 0.1])
    );
    //yew::start_app::<App>();

}
