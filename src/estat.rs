pub trait CursatBatxillerat {
    fn default() -> Self;
}
pub struct PrimerBatxillerat {
    pub català: usize,
    pub castellà: usize,
    pub anglés: usize,
    pub filosofia: usize,
    pub religió: usize,
    pub cmc: usize,
    pub ed_fisica: usize,
    pub optativa_1: usize,
    pub optativa_2: usize,
    pub optativa_3: usize,
    pub optativa_4: usize,
    pub tutoria: usize,
}

impl CursatBatxillerat for PrimerBatxillerat {
    fn default() -> Self {
        PrimerBatxillerat {
            català: 7,
            castellà: 7,
            anglés: 7,
            filosofia: 7,
            religió: 7,
            cmc: 7,
            ed_fisica: 7,
            optativa_1: 7,
            optativa_2: 7,
            optativa_3: 7,
            optativa_4: 7,
            tutoria: 7,
        }
    }
}

pub struct SegonBatxillerat {
    pub català: usize,
    pub castellà: usize,
    pub anglés: usize,
    pub h_filosofia: usize,
    pub història: usize,
    pub optativa_1: usize,
    pub optativa_2: usize,
    pub optativa_3: usize,
    pub optativa_4: usize,
    pub tutoria: usize,
}

impl CursatBatxillerat for SegonBatxillerat {
    fn default() -> Self {
        SegonBatxillerat {
            català: 7,
            castellà: 7,
            anglés: 7,
            h_filosofia: 7,
            història: 7,
            optativa_1: 7,
            optativa_2: 7,
            optativa_3: 7,
            optativa_4: 7,
            tutoria: 7,
        }
    }
}

pub struct TDR { pub nota: usize }

impl CursatBatxillerat for TDR {
    fn default() -> Self {
        TDR { nota: 7 }
    }
}


pub struct Selectivtat {
    pub fase_obligatoria: FaseObligatoria,
    pub fase_especifica: FaseEspecifica,
    pub comuna_d_opció: usize,
}
pub struct FaseObligatoria {
    pub català: usize,
    pub castellà: usize,
    pub anglés: usize,
    pub història: usize,
}
pub struct FaseEspecifica {
    pub opt_1: usize,
    pub opt_2: usize,
    pub opt_3: usize,
}

impl CursatBatxillerat for Selectivtat {
    fn default() -> Self {
        Selectivtat { 
            fase_obligatoria: FaseObligatoria { català: 7, castellà: 7, anglés: 7, història: 7 },
            fase_especifica: FaseEspecifica { opt_1: 7, opt_2: 7, opt_3: 7 },
            comuna_d_opció: 7,
        }
    }
}

impl IntoIterator for FaseEspecifica {
    type Item = usize;
    type IntoIter = std::array::IntoIter<Self::Item, 3>;

    fn into_iter(self) -> Self::IntoIter {
        <Self as IntoIterator>::IntoIter::new([self.opt_1, self.opt_2, self.opt_3])
    }
 }
