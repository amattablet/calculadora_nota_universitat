use crate::estat::{PrimerBatxillerat, SegonBatxillerat, Selectivtat, TDR};


pub fn calcular_nota_entrada_universitat(prim_batx: PrimerBatxillerat, segon_batx: SegonBatxillerat, sele: Selectivtat, tdr: TDR, ponderacions: &[f64; 4]) -> f64 {
    let nota_batxillerat_sense_tdr: usize =  {
        7 // TODO
    };

    let nota_batx = nota_batxillerat_sense_tdr as f64 * 0.9 + tdr.nota as f64 * 0.1;
    let nota_sele_obligatoria: f64 = {
        (sele.fase_obligatoria.català +
            sele.fase_obligatoria.castellà + 
            sele.fase_obligatoria.anglés +
            sele.fase_obligatoria.història +
            sele.comuna_d_opció
        ) as f64 / 5f64
    };
    let nota_sele_especifica: f64 = {
        let sequencia_de_notes: Vec<f64> = {
            let mut tmp = Vec::new();
            let esp = sele.fase_especifica.into_iter();
            tmp.push(sele.comuna_d_opció as f64 * ponderacions[0]); // Comuna d'opció
            for (i, nota) in esp.enumerate() {
                tmp.push(nota as f64 * ponderacions[i+1]);
            }
            tmp
        };

        let mut dues_mes_grans = [0f64, 0f64];

        for nota in sequencia_de_notes {
            if dues_mes_grans[0] < nota {
                dues_mes_grans[0] = nota
            } else if dues_mes_grans[1] < nota {
                dues_mes_grans[1] = nota
            }
        }

        dues_mes_grans[0] + dues_mes_grans[1]
        

    };

    return nota_batx * 0.6 + nota_sele_obligatoria * 0.4 + nota_sele_especifica;

}
